"""Stream type classes for tap-abra."""

from __future__ import annotations
from typing import Optional, Any, List, Dict

from singer_sdk import typing as th

from tap_abra.client import AbraStream
from tap_abra.client_batch import AbraBatchStream
DEFAULT_PRICE_LIST_ID = ['1000000101'] # System ID used if nothing specified. 
class CategoriesStream(AbraStream):
    """Define custom stream."""
    name = "categories"
    path = "/storemenuitems"
    primary_keys = ["ID"]
    replication_key = None
    select = True
    model_name = "Skladové menu"
    


class StorecardsStream(AbraStream):
    """Define custom stream."""
    name = "storecards"
    path = "/storecards"
    primary_keys = ["ID"]
    replication_key = None
    select = True
    model_name = "Skladové karty"
   

class StoresubcardsStream(AbraStream):
    """Define custom stream."""
    name = "storesubcards"
    path = "/storesubcards"
    primary_keys = ["ID"]
    replication_key = None
    select = True
    model_name="Dílčí skladové karty"
   

class PriceDefinitionsStream(AbraStream):
    """Define custom stream."""
    name = "price_definitions"
    path = "/pricedefinitions"
    primary_keys = ["ID"]
    replication_key = None
    select = True
    model_name = "Ceny"
    

class StorePricesStream(AbraStream):
    """Define custom stream."""
    name = "store_prices"
    path = "/storeprices"
    primary_keys = ["ID"]
    select = True
    replication_key = None
    model_name = "Položky ceníků"

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {
            "store_price_id": record["ID"],
        }

class StorePriceStream(AbraStream):
    """Define custom stream."""
    name = "store_price"
    path = "/storeprices/{store_price_id}"
    primary_keys = ["ID"]
    replication_key = None
    pagination = False
    select = False
    parent_stream_type = StorePricesStream
    model_name = "Položky ceníků"

    schema = th.PropertiesList(
        th.Property("classid", th.StringType),
        th.Property("deletedfrompricelist", th.BooleanType),
        th.Property("displayname", th.StringType),
        th.Property("id", th.StringType),
        th.Property("objversion", th.NumberType),
        th.Property("pricelist_id", th.StringType),
        th.Property("pricelistvalidity_id", th.StringType),
        th.Property("storecard_id", th.StringType),
        th.Property("pricerows", th.CustomType({"type": ["array", "string"]})),
    ).to_dict()

class SaleOrdersStream(AbraBatchStream):
    """Define custom stream."""
    name = "sale_orders"
    path = "/receivedorders"
    primary_keys = ["ID"]
    select = True
    replication_key = "createdat$date"
    model_name = "Objednávky přijaté"
   
        
    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return record["ID"]

class SaleOrderItemsStream(AbraStream):
    """Define custom stream."""
    name = "sale_order_items"
    path = "/dynsql/LG3NV0P5MJF4LBDNXVGS33PWHW/exec"
    primary_keys = ["ID"]
    rest_method = "POST"
    replication_key = None
    parent_stream_type = SaleOrdersStream
    pagination = False
    schema = th.PropertiesList(
        th.Property("name", th.StringType),
        th.Property("items", th.ArrayType(
            th.ObjectType(
                th.Property("ID", th.StringType),
                th.Property("PARENT_ID", th.StringType),
                th.Property("STORECARD_ID", th.StringType),
                th.Property("STORE_ID", th.StringType),
                th.Property("STORECARD_MAINSUPPLIER_ID", th.StringType),
                th.Property("QUANTITY", th.NumberType),
                th.Property("UNITQUANTITY", th.NumberType),
                th.Property("ROWDISCOUNT", th.NumberType),
                th.Property("UNITPRICE", th.NumberType),
                th.Property("ORIGINALUNITPRICE", th.NumberType),
                th.Property("DEALERDISCOUNT", th.NumberType),
                th.Property("TAMOUNT", th.NumberType),
                th.Property("TAMOUNTWITHOUTVAT", th. NumberType),
                th.Property("TOTALPRICE", th.NumberType),
                th.Property("RECORDER_CLOSED", th.BooleanType),
                th.Property("DELIVERYDATE$DATE", th. DateTimeType)
                # th.Property("BUSORDER_DATE$DATE", th.DateTimeType),
                # th.Property("BUSPROJECT_CLOSED", th.BooleanType),
                # th.Property("BUSPROJECT_APPROVED", th.BooleanType),
                # th.Property("BUSORDER_PROCESSSTATE", th.NumberType),
                # th.Property("BUSORDER_CLOSINGDATE$DATE", th.DateTimeType),
                # th.Property("BUSORDER_PLANNEDENDDATE$DATE", th.DateTimeType),
                # th.Property("BUSORDER_ID", th.StringType),
                # th.Property("BUSTRANSACTION_ID", th.StringType),
                # th.Property("BUSTRANSACTION_ID", th.StringType),

            )
        )),
    ).to_dict()

    def prepare_request_payload(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Optional[dict]:
        payload = {
            "select": [
                "ID",
                "PARENT_ID",
                "StoreCard_ID",
                "Store_ID",
                "StoreCard_MainSupplier_ID",
                "Quantity",
                "UnitQuantity",
                "RowDiscount",
                "UnitPrice",
                "OriginalUnitPrice",
                "DealerDiscount",
                "TAmount",
                "TAmountWithoutVAT",
                "TotalPrice",
                "RecOrder_Closed",
                "DeliveryDate$DATE"
                # "BusOrder_Date$DATE",
                # "BusProject_Closed",
                # "BusProject_Approved",
                # "BusOrder_ProcessState",
                # "BusOrder_ClosingDate$DATE",
                # "BusOrder_PlannedEndDate$DATE"
            ],
            "where": [
                {
                    "id": "PARENT_ID",
                    "value": context["orders_ids"]
                }
            ]
        }
        return payload

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        return params

class PurchaseOrdersStream(AbraBatchStream):
    """Define custom stream."""
    name = "purchase_orders"
    path = "/issuedorders"
    primary_keys = ["ID"]
    select = True
    replication_key = "createdat$date"
    model_name = "Objednávky vydané"

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return record["ID"]

class PurchasesOrderItemsStream(AbraStream):
    """Define custom stream."""
    name = "purchase_order_items"
    path = "/dynsql/VMULTBGPCPOOVBH15ECR0XS0TW/exec"
    primary_keys = ["ID"]
    rest_method = "POST"
    replication_key = None
    parent_stream_type = PurchaseOrdersStream
    pagination = False
    schema = th.PropertiesList(
        th.Property("name", th.StringType),
        th.Property("items", th.ArrayType(
            th.ObjectType(
                th.Property("ID", th.StringType),
                th.Property("PARENT_ID", th.StringType),
                th.Property("STORECARD_ID", th.StringType),
                th.Property("STORE_ID", th.StringType),
                th.Property("STORECARD_MAINSUPPLIER_ID", th.StringType),
                th.Property("QUANTITY", th.NumberType),
                th.Property("DELIVEREDQUANTITY", th.NumberType),
                th.Property("UNITQUANTITY", th.NumberType),
                th.Property("ROWDISCOUNT", th.NumberType),
                th.Property("UNITPRICE", th.NumberType),
                th.Property("ORIGINALUNITPRICE", th.NumberType),
                th.Property("TAMOUNT", th.NumberType),
                th.Property("TAMOUNTWITHOUTVAT", th.NumberType),
                th.Property("TOTALPRICE", th.NumberType),
                th.Property("DELIVERYDATE$DATE", th.DateTimeType)
            )
        )),
    ).to_dict()

    def prepare_request_payload(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Optional[dict]:
        payload = {
            "select": [
                "ID",
                "PARENT_ID",
                "StoreCard_ID",
                "Store_ID",
                "StoreCard_MainSupplier_ID",
                "Quantity",
                "deliveredquantity",
                "UnitQuantity",
                "RowDiscount",
                "UnitPrice",
                "OriginalUnitPrice",
                "TAmount",
                "TAmountWithoutVAT",
                "TotalPrice",
                "DeliveryDate$DATE"
            ],
            "where": [
                {
                    "id": "PARENT_ID",
                    "value": context["orders_ids"]
                }
            ]
        }
        return payload
    
    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        return params
    
class StoresStream(AbraBatchStream):
    """Define custom stream."""
    name = "stores"
    path = "/stores"
    primary_keys = ["ID"]
    select = True
    pagination = False
    model_name = "Sklady"

class SuppliersStream(AbraBatchStream):
    """Define custom stream."""
    name = "suppliers"
    path = "/suppliers"
    primary_keys = ["ID"]
    select = True
    model_name = "Dodavatelé"

class ReceiptCardsStream(AbraBatchStream):
    """Define custom stream."""
    name = "receipt_cards"
    path = "/receiptcards"
    primary_keys = ["ID"]
    select = "ID,createdat$date,docdate$date,displayname,currency_id,currrate"
    replication_key = "createdat$date"
    schema = th.PropertiesList(
        th.Property("ID", th.StringType),
        th.Property("createdat$date", th.DateTimeType),
        th.Property("docdate$date", th.BooleanType),
        th.Property("displayname", th.StringType),
        th.Property("currency_id", th.StringType),
        th.Property("currrate", th.NumberType),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return record["ID"]

class ReceiptCardItemsStream(AbraStream):
    """Define custom stream."""
    name = "receipt_card_items"
    path = "/dynsql/WBFDIVPW1ZE13HBT00C5OG4NF4/exec"
    primary_keys = ["ID"]
    rest_method = "POST"
    replication_key = None
    parent_stream_type = ReceiptCardsStream
    pagination = False
    schema = th.PropertiesList(
        th.Property("name", th.StringType),
        th.Property("items", th.ArrayType(
            th.ObjectType(
                th.Property("ID", th.StringType),
                th.Property("PARENT_ID", th.StringType),
                th.Property("STORECARD_ID", th.StringType),
                th.Property("STORE_ID", th.StringType),
                th.Property("STORECARD_MAINSUPPLIER_ID", th.StringType),
                th.Property("QUANTITY", th.NumberType),
                th.Property("UNITQUANTITY", th.NumberType),
                th.Property("UNITPRICE", th.NumberType),
                th.Property("ORIGINALUNITPRICE", th.NumberType),
                th.Property("TAMOUNT", th.NumberType),
                th.Property("TOTALPRICE", th.NumberType),
                th.Property("FIRMS_CURRENCY_ID", th.StringType),
                th.Property("FIRMS_ID", th.StringType),
                th.Property("FIRMS_CODE", th.StringType),
                th.Property("FIRMS_NAME", th.StringType),
                th.Property("PROVIDE_ID", th.StringType),
                th.Property("PROVIDEROW_ID", th.StringType)
            )
        )),
    ).to_dict()

    def prepare_request_payload(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Optional[dict]:
        payload = {
            "select": [
                "ID",
                "PARENT_ID",
                "StoreCard_ID",
                "Store_ID",
                "StoreCard_MainSupplier_ID",
                "UnitQuantity",
                "Quantity",
                "UnitPrice",
                "OriginalUnitPrice",
                "UnitPrice",
                "OriginalUnitPrice",
                "TAmount",
                "TotalPrice",
                "Firms_Currency_ID",
                "Firms_ID",
                "Firms_Code",
                "Firms_Name",
                "Provide_ID",
                "ProvideRow_ID"
            ],
            "where": [
                {
                    "id": "PARENT_ID",
                    "value": context["orders_ids"]
                }
            ]
        }
        return payload
    
    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        return params

# Firms
class FirmsStream(AbraStream):
    """Define custom stream."""
    name = "firms"
    path = "/firms"
    primary_keys = ["ID"]
    replication_key = None
    select = True
    model_name = "Adresář"  

# issued invoices
class IssuedInvoicesStream(AbraBatchStream):
    name = "issued_invoices"
    path = "/issuedinvoices"
    primary_keys = ["ID"]
    select = True
    replication_key = "createdat$date"
    model_name = "Faktury vydané"

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return record["ID"]

# This is not dynsql, but just QUERY. To get issued invoice lines.
class IssuedInvoiceItemsStream(AbraStream):
    """Define custom stream."""
    name = "issued_invoice_items"
    path = "/query"
    primary_keys = ["ID"]
    rest_method = "POST"
    replication_key = None
    parent_stream_type = IssuedInvoicesStream
    pagination = False
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("parent_id", th.StringType),
        th.Property("storecard_id", th.StringType),
        th.Property("store_id", th.StringType),
        th.Property("unitquantity", th.NumberType),
        th.Property("unitprice", th.NumberType),
        th.Property("tamountwithoutvat", th.NumberType),
        th.Property("provide_id", th.StringType),
        th.Property("providerow_id", th.StringType),
        th.Property("isanydiscount", th.BooleanType),
    ).to_dict()  

    def prepare_request_payload(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Optional[dict]:
        parent_ids = ", ".join(context.get('orders_ids'))
        orders_ids = context.get('orders_ids')
        quoted_orders = ["'" + str(order_id) + "'" for order_id in orders_ids]
        result = ", ".join(quoted_orders)

        payload = {
            "class": "OBBDOKTWEFD13ACM03KIU0CLP4",
            "select": [
                "id",
                "parent_id",
                "storecard_id",
                "store_id",
                "unitquantity",
                "unitprice",
                "tamountwithoutvat",
                "provide_id",
                "providerow_id",
                "isanydiscount"
            ],
            "where": f"parent_id in ({result})"
        }
        return payload

class DealerDiscounts(AbraStream):
    """Define custom stream."""
    name = "dealerdiscounts"
    path = "/dealerdiscounts"
    primary_keys = ["ID"]
    replication_key = None
    select = True
    model_name = "Slevy dealerské"

class DealerCategories(AbraStream):
    """Define custom stream."""
    name = "dealercategories"
    path = "/dealercategories"
    primary_keys = ["ID"]
    replication_key = None
    select = True
    model_name = "Dealerské třídy"

class PriceListsStream(AbraBatchStream):
    """Define custom stream."""
    name = "price_lists"
    path = "/pricelists"
    primary_keys = ["ID",'code']
    select = True
    replication_key = None
    model_name = "Ceníky"
   
        
    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {"price_list_id":record["ID"],"price_code":record["code"]}    
class StorePricesPriceListStream(AbraStream):
    """Define custom stream."""
    name = "store_prices_selected_pricelist"
    path = "/query"
    primary_keys = ["ID"]
    rest_method = "POST"
    replication_key = None
    pagination = True
    skip = 0
    parent_stream_type = PriceListsStream
    schema = th.PropertiesList(
        th.Property("ID", th.StringType),
        th.Property("Parent_ID", th.StringType),
        th.Property("Price_ID", th.StringType),
        th.Property("Amount", th.NumberType),
        th.Property("Qunit", th.StringType),
        th.Property("Unitrate", th.NumberType),
        th.Property("Parent_ID.Pricelist_ID", th.StringType),
        th.Property("Parent_ID.Storecard_ID", th.StringType),
    ).to_dict()

    def prepare_request_payload(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Optional[dict]:
        pricelist_ids = []
        prices_codes = self.config.get("pricelist_codes",None)
        if prices_codes:
            # support comma separated str
            if type(prices_codes) == str:
                prices_codes = prices_codes.split(",")
            #fetch prices_ids against prices_codes
            prices_ids = context.get("orders_ids")
            for price_id in prices_ids:
                if price_id['price_code'] in prices_codes:
                    pricelist_ids.append(price_id['price_list_id'])
        if not pricelist_ids:
            pricelist_ids = DEFAULT_PRICE_LIST_ID

        # Convert list to SQL string
        pricelist_ids_sql_string = ', '.join(f"'{element}'" for element in pricelist_ids)
        payload = {
            "class": "GHYLVQXQ3FE13DQC01C0CX3F40",
            "select": [
               "ID",
                "Parent_ID",
                "Price_ID",
                "Amount",
                "Unitrate",
                "Qunit",
                "Parent_ID.Pricelist_ID",
                "Parent_ID.Storecard_ID"
            ],
            "where": f"Parent_ID.Pricelist_ID in ({pricelist_ids_sql_string})",
            "skip": self.skip, # paginate
            "take": self.items_per_page
        }
        return payload

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        if next_page_token:
            self.skip = next_page_token
        return params
