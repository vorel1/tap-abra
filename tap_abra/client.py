"""REST client handling, including AbraStream base class."""

import requests
import datetime
from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable, Callable
from singer_sdk import typing as th
from memoization import cached

from singer_sdk.helpers.jsonpath import extract_jsonpath
from singer_sdk.streams import RESTStream
from singer_sdk.authenticators import BasicAuthenticator
from singer_sdk.exceptions import RetriableAPIError, FatalAPIError
import backoff
from backports.cached_property import cached_property

class FailJobException(Exception):
    pass
class AbraStream(RESTStream):
    """Abra stream class."""
    error_counter = 0
    models_list = []

    @property
    def url_base(self) -> str:
        url = self.config.get("url_base")
        #Check if there's no http or https and add it
        if not url.startswith("https://") and not url.startswith("http://"):
            url = f"https://{url}"
        return url

    records_jsonpath = "$[*]"

    _requests_session = requests.Session()
    pagination = True
    items_per_page = 1000
    select = None

    @property
    def authenticator(self) -> BasicAuthenticator:
        """Return a new authenticator object."""
        self._requests_session.verify = False
        return BasicAuthenticator.create_for_stream(
            self,
            username=self.config.get("username"),
            password=self.config.get("password"),
        )

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        return headers
    
    def request_decorator(self, func: Callable) -> Callable:
        decorator: Callable = backoff.on_exception(
            backoff.expo,
            (
                RetriableAPIError,
                requests.exceptions.ReadTimeout,
                requests.exceptions.ConnectionError,
                requests.exceptions.RequestException,
                ConnectionRefusedError
            ),
            max_tries=7,
            factor=2,
        )(func)
        return decorator

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        res_json = response.json()
        if self.pagination:
            previous_token = previous_token or 0
            if len(res_json) == 0:
                next_page_token = None
            else:
                next_page_token = previous_token + self.items_per_page
            return next_page_token
        return None

    def validate_response(self, response: requests.Response) -> None:
        """Validate HTTP response."""
        if response.status_code == 401:
            raise FatalAPIError(
                f"Unauthorized: {response.status_code} {response.reason} at {self.path}"
            )
        
        if response.status_code == 404:
            msg = (
                f"{response.status_code} Server Error: "
                f"{response.reason} for path: {response.url}"
            )
            raise FailJobException(msg)
        #need to set the local time invalid check here because the response code is coming back as 500
        if response.status_code >= 400 and "local time is invalid" in response.text:
            self.error_counter += 1
        elif response.status_code == 500:
            #For this branch we need the job to fail on 500
            raise Exception(response.text)
        
        if response.status_code >= 400 and self.config.get("ignore_server_errors", True):
            self.error_counter += 1
        elif response.status_code >= 400 and "local time is invalid" in response.text:
            pass # need to add this check again so that we don't fail the job on local time being invalid
        elif 500 <= response.status_code < 600 or response.status_code in [429, 403]:
            msg = (
                f"{response.status_code} Server Error: "
                f"{response.reason} for path: {self.path}"
            )
            raise RetriableAPIError(msg)
        elif 400 <= response.status_code < 500:
            msg = (
                f"{response.status_code} Client Error: "
                f"{response.reason} for path: {self.path}"
            )
            raise FatalAPIError(msg)
        try:
            response.json()
        except:
            raise RetriableAPIError(f"Invalid JSON: {response.text}")

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        #No query parameters for this stream
        if self.name=="issued_invoice_items":
            return params
        params["orderby"] = "ID"
        if self.select:
            params["select"] = ",".join(self.selected_properties or self.primary_keys)
        if self.pagination:
            params["take"] = self.items_per_page
            params["skip"] = next_page_token
        if self.replication_key:
            self.start_date = self.get_starting_timestamp(context)
            if self.start_date:
                start_date = self.start_date - datetime.timedelta(days=180)
                params["where"] = f"{self.replication_key} gt timestamp'{start_date}'"
        return params

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        """Parse the response and return an iterator of result rows."""
        if response.status_code>=400 and self.config.get("ignore_server_errors", True):
            return []
        else:
            yield from extract_jsonpath(self.records_jsonpath, input=response.json())

    @backoff.on_exception(
        backoff.expo,
        (RetriableAPIError),
        max_tries=10,
        factor=3,
    )
    def make_models_request(self,model_id=None):
        headers = self.http_headers
        authenticator = self.authenticator
        if authenticator:
            headers.update(authenticator.auth_headers or {})
        url = f"{self.url_base}/dynsql"
        if model_id is not None:
            url = f"{url}/{model_id}"

        resp = requests.get(url=url,headers=headers,verify=False)
        if resp.status_code == 404:
            self.logger.warn("Unable to fetch models from dynsql endpoint")
            msg = (
                f"{resp.status_code} Server Error: "
                f"{resp.reason} for path: {resp.url}"
            )
            raise Exception(msg)
        self.validate_response(resp)
        if model_id is None:
            if resp.status_code == 200:
                self.models_list = resp.json()
                return self.models_list
        if model_id:
            if resp.status_code == 200:
                return resp.json().get("datasets",[])
            
        raise Exception("Unable to find Model Definitions")

    def fetch_models(self):
        if len(self.models_list)>0:
            return self.models_list

        self.models_list = self.make_models_request()
        return self.models_list
    
    def get_model_fields_list(self,model_id):
        resp = self.make_models_request(model_id=model_id)
        fields = []
        if len(resp):
            for data in resp:
                #Only Main dataset fields seems to be working
                if data.get("name") == "MAIN":
                    fields+= data['fields']
        return fields            
    def extract_type(self,datatype):
        if datatype in ['dtString']:
            return th.StringType
        if datatype in ['dtBoolean']:
            return th.BooleanType
        if datatype in ['dtInteger','dtFloat']:
            return th.NumberType
        if datatype in ['dtDateTime']:
            return th.DateTimeType
        if datatype in ['dtDate']:
            return th.DateType
        
        
    def prepare_schema(self,fields):
        properties = []
        if len(fields)>0:
            for field in fields:
                if field.get("mastermonikerpclsid") and field.get("prefixedfield") is False: #This is a weird relation not all fields work in GET
                    type = self.extract_type(field['datatype'])

                    field_name = field.get("fieldname")
                    if field_name is not "ID":
                        field_name = field_name.lower() 
                    property = th.Property(field_name, type)
                    if type:
                        properties.append(property)

        if len(properties)>0:            
            return th.PropertiesList(*properties).to_dict()
        else:
            raise Exception(f"Unable to prepare schema for stream {self.name}")

    def get_custom_field_definition(self,field_name,data_type="dtString",prefix_field=False):
        return {
                        "fieldname":field_name,#Whatever case we send is returned in response. So use ID as tap is already assuming it.
                        "datatype":data_type,
                        "mastermonikerpclsid":"Some random value for prepare_schema function",
                        "prefixedfield": prefix_field

                    }
    def get_additional_stream_fields(self,current_fields,model_name):
        if model_name in ["Skladové menu"]:
            current_fields += [
                self.get_custom_field_definition("displayname"),
                self.get_custom_field_definition("displayparent"),
                self.get_custom_field_definition("fullpath"),
                self.get_custom_field_definition("classid"),
            ]
        else:
            current_fields += [
                self.get_custom_field_definition("displayname"),
                self.get_custom_field_definition("classid"),
            ]
        return current_fields    

    def get_model_schema(self,model_name):
        models_list = self.fetch_models()
        for model in models_list:
            if model.get("name") == model_name:
                fields_list = self.get_model_fields_list(model.get("id"))
                if fields_list:
                    fields_list.append(
                        self.get_custom_field_definition("ID")#Whatever case we send is returned in response. So use ID as tap is already assuming it.
                        )
                    fields_list = self.get_additional_stream_fields(fields_list,model_name)
                    return self.prepare_schema(fields_list)
                
        raise Exception(f"Unable to prepare schema for stream {self.name}")   

    @cached_property
    def selected_properties(self):
        selected_properties = []
        for key, value in self.metadata.items():
            if isinstance(key, tuple) and len(key) == 2 and value.selected:
                field_name = key[-1]
                selected_properties.append(field_name)
        return selected_properties  

    @cached_property
    def schema(self):
        return self.get_model_schema(self.model_name)   
                