"""Abra tap class."""

from typing import List

from singer_sdk import Tap, Stream
from singer_sdk import typing as th  
from tap_abra.streams import (
    CategoriesStream,
    StorecardsStream,
    StoresubcardsStream,
    PriceDefinitionsStream,
    StorePricesStream,
    StorePriceStream,
    SaleOrdersStream,
    SaleOrderItemsStream,
    PurchaseOrdersStream,
    PurchasesOrderItemsStream,
    StoresStream,
    SuppliersStream,
    ReceiptCardsStream,
    ReceiptCardItemsStream,
    FirmsStream,
    IssuedInvoicesStream,
    IssuedInvoiceItemsStream,
    DealerDiscounts,
    DealerCategories,
    PriceListsStream,
    StorePricesPriceListStream
)

STREAM_TYPES = [
    CategoriesStream,
    StorecardsStream,
    StoresubcardsStream,
    PriceDefinitionsStream,
    StorePricesStream,
    StorePriceStream,
    SaleOrdersStream,
    SaleOrderItemsStream,
    PurchaseOrdersStream,
    PurchasesOrderItemsStream,
    StoresStream,
    SuppliersStream,
    ReceiptCardsStream,
    ReceiptCardItemsStream,
    FirmsStream,
    IssuedInvoicesStream,
    IssuedInvoiceItemsStream,
    DealerDiscounts,
    DealerCategories,
    PriceListsStream,
    StorePricesPriceListStream,
]


class TapAbra(Tap):
    """Abra tap class."""
    name = "tap-abra"

    config_jsonschema = th.PropertiesList(
        th.Property(
            "username",
            th.StringType,
            required=True,
        ),
        th.Property(
            "password",
            th.StringType,
            required=True,
        ),
        th.Property(
            "start_date",
            th.DateTimeType,
            description="The earliest record date to sync"
        )
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]

if __name__ == "__main__":
    TapAbra.cli()
